COPTIMIZE = -O -march=native
CFLAGS = -Wall -Wextra -Wmissing-prototypes -pedantic -std=gnu11 $(COPTIMIZE)
ifeq ($(OS),Win_NT)
CFLAGS += -mconsole
endif

TARGET = slowprint
ifeq ($(OS),Win_NT)
TARGET := $(TARGET).exe
endif

SOURCES = slowprint.c

.PHONY: all
all: $(TARGET)

$(TARGET): $(SOURCES)
	$(CC) $(CFLAGS) -o $@ $^

clean:
	-$(RM) $(TARGET)
