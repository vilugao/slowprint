#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

#define QUOTE(str) #str
#define DEFAULT_BPS 9600

#ifdef _WIN32
	typedef DWORD mytime_t;
	#define ttsleep(tt)  Sleep(tt), 0
#else
	typedef struct timespec mytime_t;
	#define ttsleep(tt)  nanosleep(&tt, NULL)
#endif

static void set_speed(
	const unsigned long bps,
	unsigned int * const n_bytes_interval,
	mytime_t * const tt
) {
	unsigned long microseconds;
	if (bps >= 8000) {
		/* x Bytes per 10 ms. */
		*n_bytes_interval = bps / 800;
		microseconds = 10000;
	} else if (bps >= 800) {
		/* x Bytes per y µs. */
		microseconds = (*n_bytes_interval = (bps + 400) / 800)
			* 8000000UL / bps;
	} else {
		/* 1 Byte per x µs. */
		*n_bytes_interval = 1;
		microseconds = 8000000UL / bps;
	}
#ifdef _WIN32
	*tt = microseconds / 1000;
#else
	tt->tv_sec = microseconds / 1000000UL;
	tt->tv_nsec = microseconds * 1000;
#endif
}

static const char *progname;

static void print_help(FILE * const out)
{
	fprintf(out, "Usage: %s [n] [-v|/V]\n\n", progname);
	fputs("  [n]     - Limit transfer rate in bits per seconds. (Default: " QUOTE(DEFAULT_BPS) ")\n", out);
	fputs("  [-v|/V] - Turn the debug verbosity on.\n", out);
}

#define FLAG_HELP (1 << 0)
#define FLAG_DEBUG (1 << 1)

static int parse_args(
	int argc, const char **argv,
	unsigned long *bps, unsigned char *flags
) {
	const char *argvstr;
	while (--argc) {
		argvstr = *++argv;
		if (
			argvstr[2] == '\0'
			&& (argvstr[0] == '-' || argvstr[0] == '/')
		) {
			switch (toupper(argvstr[1])) {
			case 'H': /* Help */
				*flags |= FLAG_HELP;
				break;
			case 'V': /* Verbose / Debug */
				*flags |= FLAG_DEBUG;
				break;
			default:
				return argc;
			}
		} else {
			*bps = strtoul(argvstr, NULL, 10);
		}
	}
	return 0;
}

int main(const int argc, const char *argv[])
{
	unsigned long bps = DEFAULT_BPS;
	unsigned char flags = 0;

	progname = argv[0];

	if (parse_args(argc, argv, &bps, &flags) || !bps) {
		print_help(stderr);
		exit(EXIT_FAILURE);
	}

	if (flags & FLAG_HELP) {
		print_help(stdout);
		exit(EXIT_SUCCESS);
	}

	mytime_t tt;
	unsigned int bytes_per_t, remaining_bytes = 0;

	set_speed(bps, &bytes_per_t, &tt);

	if (flags & FLAG_DEBUG) {
		fputs("#---\n", stderr);
		fprintf(stderr, "%s = %lu;\n", "bps", bps);
		fprintf(stderr, "%s = %u;\n", "bytes_per_t", bytes_per_t);
#ifdef _WIN32
		fprintf(stderr, "%s = %lu;\n", "dwMilliseconds", tt);
#else
		fprintf(stderr, "%s = %li;\n", "tv_sec", tt.tv_sec);
		fprintf(stderr, "%s = %li;\n", "tv_nsec", tt.tv_nsec);
#endif
		fputs("#---\n", stderr);
		fflush(stderr);
	}

	{
		int c;
		while ((c = getchar()) != EOF) {
			if (!remaining_bytes) {
				remaining_bytes = bytes_per_t;
				if (fflush(stdout) != 0)
					break;
				if (ttsleep(tt) != 0) {
					perror("nanosleep");
					exit(EXIT_FAILURE);
				}
			}
			if (putchar(c) == EOF)
				break;
			remaining_bytes--;
		}
	}

	if (ferror(stdin)) {
		perror("stdin");
		exit(EXIT_FAILURE);
	}
	if (ferror(stdout)) {
		perror("stdout");
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}
